//middleware
function logRequests(req, res, next) {
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

    console.log('%s: %s %s, ', ip, req.method, req.originalUrl, req.body);
    next();
}

function log(msg, data) {
    if (data) {
        console.log(msg, data);
    } else {
        console.log(msg);
    }
}

module.exports = {
    logRequests: logRequests,
    log: log
}