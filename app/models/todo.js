var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TodoSchema = new Schema({
    name: String,
    completed: Boolean,
    note: String,
    updated_at: { type: Date, default: Date.now }
});
/*
data types: 
String
Boolean
Date
Array
Number
ObjectId
Mixed
Buffer
 */

var model = mongoose.model('Todo', TodoSchema);

function getTasks(onOk, onError) {
    model.find(function (err, todos) {
        if (err) {
            onError(err);
        } else {
            onOk(todos);
        }
    });

    /*
    Model.find(conditions, [fields], [options], [callback])
    Model.findById(id, [fields], [options], [callback])
    Model.findOne(conditions, [fields], [options], [callback])

    // Get ONLY completed tasks
    model.find({ completed: true }, callback);
    // Get all tasks ending with `JS`
    model.find({ name: /JS$/ }, callback);

    var oneYearAgo = new Date();
    oneYearAgo.setYear(oneYearAgo.getFullYear() - 1);
    // Get all tasks staring with `Master`, completed
    Todo.find({ name: /^Master/, completed: true }, callback);
    // Get all tasks staring with `Master`, not completed and created from year ago to now...
    Todo.find({ name: /^Master/, completed: false }).where('updated_at').gt(oneYearAgo).exec(callback);
    */
}
function getTask(id, onOk, onError) {
    model.findById(id, function (err, todo) {
        if (err) {
            onError(err);
        } else {
            onOk(todo);
        }
    });
}
function updateTask(id, name, onOk, onError) {
    getTask(id, function (todo) {
        todo.name = name;
        createTask(todo, function () {
            onOk({
                message: 'Task updated'
            });
        }, onError);
    }, onError);

    /*
    Model.update(conditions, update, [options], [callback])
    Model.findByIdAndUpdate(id, [update], [options], [callback])
    Model.findOneAndUpdate([conditions], [update], [options], [callback])

    // update `multi`ple tasks from complete false to true
    model.update({ name: /master/i }, { completed: true }, { multi: true }, callback);

    model.findOneAndUpdate({name: /JS$/ }, {completed: false}, callback);
    */
}
function deleteTask(id, onOk, onError) {
    var request = {
        _id: id
    }
    model.remove(request, function (err, todo) {
        if (err) {
            onError(err);
        } else {
            onOk({
                message: 'Task deleted'
            });
        }
    });

    /*
    Model.remove(conditions, [callback])
    Model.findByIdAndRemove(id, [options], [callback])
    Model.findOneAndRemove(conditions, [options], [callback])
    */
}
function createTask(model, onOk, onError) {
    model.save(function (err) {
        if (err) {
            onError(err);
        } else {
            onOk({
                message: 'Task created'
            });
        }
    })
}

module.exports = {
    model: model,

    getTasks: getTasks,
    getTask: getTask,
    updateTask: updateTask,
    deleteTask: deleteTask,
    createTask: createTask
}