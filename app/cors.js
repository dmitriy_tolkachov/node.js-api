// http://stackoverflow.com/questions/21455045/angularjs-http-cors-and-http-authentication
// http://enable-cors.org/server_expressjs.html

function handleCORS(req, res, next) {
    /**
     * Response settings
     * @type {Object}
     */
    var responseSettings = {
        "AccessControlAllowOrigin": req.headers.origin,
        //will be 2 requests: OPTIONS and real request
        //"AccessControlAllowHeaders": "Content-Type,X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name",
        //will be 1 real request 
        //"AccessControlAllowHeaders": "Authorization",
        
        "AccessControlAllowHeaders": "Accept, Content-Type, Origin, X-Requested-With, Authorization",
        "AccessControlAllowMethods": "POST, PUT, DELETE, GET, OPTIONS",
        "AccessControlAllowCredentials": true
    };

    /**
     * Headers
     */
    res.header("Access-Control-Allow-Origin", "*");
    //required for allowing authentication header (otherwise OPTIONS request will fail)
    res.header("Access-Control-Allow-Headers", (req.headers['access-control-request-headers']) ? req.headers['access-control-request-headers'] : responseSettings.AccessControlAllowHeaders);
    

    res.header("Access-Control-Allow-Methods", (req.headers['access-control-request-method']) ? req.headers['access-control-request-method'] : responseSettings.AccessControlAllowMethods);
    //res.header("Access-Control-Allow-Credentials", responseSettings.AccessControlAllowCredentials);
    //res.header("Access-Control-Allow-Origin", responseSettings.AccessControlAllowOrigin);

    res.header("X-Execution-Time", "");
    res.removeHeader("Connection");

    if ('OPTIONS' == req.method) {
        res.send(200);
    }
    else {
        next();
    }
}

/*//option 2
function handleCORS(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
}*/

module.exports = handleCORS;