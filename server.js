var APP_PORT = process.env.PORT || 8080;
var APP_PORT_SSL = 8443;

// BASE SETUP
// =============================================================================

var express = require('express');
var app = express();

var bodyParser = require('body-parser');

/*var mongoose = require('mongoose');
mongoose.Promise = global.Promise; // Use native Node promises
mongoose
    .connect('mongodb://localhost:27017/api')
    .then(() =>  console.log('connection succesful'))
    .catch((err) => console.error(err));

var Todo = require('./app/models/todo');*/


var Logger = require('./app/logger');
var handleCORS = require('./app/cors');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

/**
 * CORS
 * On all requests add headers
 */
//app.all('*', handleCORS);
app.use(handleCORS);

app.set('etag', false); //will not be 'ETag' in response header
app.disable('x-powered-by'); //will not be 'X-Prowered-By: Express' in response header

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();

router.use(Logger.logRequests);

router.get('/', function (req, res) {
    var obj = {
        message: 'hooray! welcome to our api!'
    };
    res.json(obj);
});

router.get('/countries', function (req, res) {
    var countries = require('./data/countries.json');
    res.json(countries);
});
router.get('/airport-terminals', function (req, res) {
    var country = req.query.country_code; //req.params.country_code
    var airportTerminals = require('./data/airport-terminals_' + country + '.json');

    res.json(airportTerminals);
});

/*
router.route('/tasks')
    //create new entity
    .post(function (req, res) {
        //var todo = new Todo.model({name: 'Master NodeJS', completed: false, note: 'Getting there...'});
        var todo = new Todo.model();
        todo.name = req.body.name;

        Logger.log(todo.name);

        Todo.createTask(todo, function () {
            res.json({ message: 'Task created.' })
        }, function (err) {
            res.send(err);
        });
    })
    //get all entities
    .get(function (req, res) {
        Todo.getTasks(function (items) {
            res.json(items);
        }, function (err) {
            res.send(err);
        });
    })

router.route('/tasks/:task_id')
    //get 1 entity
    .get(function (req, res) {
        Todo.getTask(req.params.task_id, function (item) {
            res.json(item);
        }, function (err) {
            res.send(err);
        });
    })
    //update 1 entity
    .put(function (req, res) {
        Todo.updateTask(req.params.task_id, req.body.name, function (data) {
            res.json(data);
        }, function (err) {
            res.send(err);
        });
    })
    //delete 1 entity
    .delete(function (req, res) {
        Todo.deleteTask(req.params.task_id, function (data) {
            res.json(data);
        }, function (err) {
            res.send(err);
        });
    })
*/

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
// http://stackoverflow.com/questions/11744975/enabling-https-on-express-js
// http://blog.mgechev.com/2014/02/19/create-https-tls-ssl-application-with-express-nodejs/
// http://www.selfsignedcertificate.com/
var fs = require('fs');
var http = require('http');
var https = require('https');
var privateKey  = fs.readFileSync('./sslcert/server.key', 'utf8');
var certificate = fs.readFileSync('./sslcert/server.cert', 'utf8');

var credentials = {key: privateKey, cert: certificate};

var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

httpServer.listen(APP_PORT, function(){
    console.log('http on ' + APP_PORT);
});
httpsServer.listen(APP_PORT_SSL, function(){
    console.log('https on port ' + APP_PORT_SSL);
});
//app.listen(APP_PORT);