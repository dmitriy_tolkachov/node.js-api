# simple node.js api application #

to read: 
* https://scotch.io/tutorials/build-a-restful-api-using-node-and-express-4
* http://adrianmejia.com/blog/2014/10/01/creating-a-restful-api-tutorial-with-nodejs-and-mongodb/
* https://stormpath.com/blog/tutorial-build-rest-api-mobile-apps-using-node-js
* https://www.tutorialspoint.com/nodejs/nodejs_restful_api.htm
* http://blog.modulus.io/nodejs-and-express-create-rest-api
* https://habrahabr.ru/post/193458/

#### How to run ####
* install node, nodemon, mongodb globally
* go to project directory and install dependencies: `npm install`
* run mongodb: `mongod` // C:\Program Files\MongoDB\Server\3.2\bin>mongod --dbpath d:\dev\db
* run app: `npm start`
* browse `http://localhost:8080/api`

#### API endpoints: ####
* GET http://localhost:8080/api

* GET http://localhost:8080/api/tasks
* POST http://localhost:8080/api/tasks
* GET http://localhost:8080/api/tasks/57eb8bdfb2c7f818ac5257e7
* PUT http://localhost:8080/api/tasks/57eb8bdfb2c7f818ac5257e7
* DELETE http://localhost:8080/api/tasks/57eb8bdfb2c7f818ac5257e7
